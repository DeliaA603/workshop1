import sys

print(20 * '-')
print('File Editor v 2.0')
print('-' * 20)

print('something new here')

if len(sys.argv) == 2:
    nume_fisier = sys.argv[1]
    print(nume_fisier)
else:
    nume_fisier = input('nume fisier: ')


with open(nume_fisier, mode='a') as handler:
    line_nr = 1

    while True:
    line = input(line_nr)
    line_nr +=1
    if line == '.':
        break
    handler.write(line  + '\n')
    
